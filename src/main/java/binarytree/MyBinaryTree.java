package binarytree;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class MyBinaryTree<K extends Comparable, V> implements Map<K, V> {

    private int size;
    private Node<K, V> root;

    //BinaryTree class isn`t finished

    public MyBinaryTree() {
        this.root = null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public boolean containsKey(Object key) {
        Node<K, V> currentNode = root;
        while (currentNode.getKey().compareTo(key) != 0) {
            if (currentNode.compareTo(key) < 0) {
                currentNode = currentNode.left;
            } else {
                currentNode = currentNode.right;
            }
            if (currentNode == null) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        return null;
    }

    @Override
    public V put(K key, V value) {
        Node<K, V> newNode = new Node<>(key, value);
        size++;
        return null;
    }

    @Override
    public V remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }
}

package binarytree;

import java.util.Map;

public class Node<K extends Comparable, V> implements Map.Entry<K, V> {

    K key;
    V value;

    Node<K, V> left;
    Node<K, V> right;

    public Node(K key, V value) {
        this.key = key;
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public int compareTo(K k) {
        return this.key.compareTo(k);
    }

    public int compareTo(Object k) {
        return this.key.compareTo(k);
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public V setValue(V value) {
        return this.value = value;
    }
}

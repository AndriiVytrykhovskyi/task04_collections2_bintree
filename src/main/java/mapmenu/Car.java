package mapmenu;

import java.util.Objects;

public class Car {
    private String model;
    private String make;
    private int price;
    private double volume;

    Car(String model, String make, int price, double volume) {
        this.model = model;
        this.make = make;
        this.price = price;
        this.volume = volume;
    }

    String getModel() {
        return model;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return price == car.price &&
                Double.compare(car.volume, volume) == 0 &&
                Objects.equals(model, car.model) &&
                Objects.equals(make, car.make);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model, make, price, volume);
    }

    @Override
    public String toString() {
        return "Model='" + model + '\'' +
                ", make='" + make + '\'' +
                ", price=" + price +
                ", volume=" + volume;
    }
}

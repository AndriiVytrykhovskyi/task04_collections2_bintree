package mapmenu;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

class Dealer {
    private Set<Car> cars = new HashSet<>(200, 0.75f);

    Set<Car> getAll() {
        if (cars.size() == 0) {
            System.out.println("Its empty");
        }
        return cars;
    }

    void findByModel(String model) {
        Set<Car> collect = cars.stream()
                .filter(c -> c.getModel().equalsIgnoreCase(model))
                .collect(Collectors.toSet());
        collect.forEach(System.out::println);
        if (collect.size() == 0) {
            System.out.println("There are no cars with this model");
        }
    }

    boolean add(Car car) {
        return cars.add(car);
    }

}

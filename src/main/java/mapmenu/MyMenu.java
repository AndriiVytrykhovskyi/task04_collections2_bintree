package mapmenu;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

class MyMenu {

    private Map<String, String> menu;
    private Map<String, Runnable> menuMethods;
    private static Scanner sc = new Scanner(System.in);
    private static Dealer dealer = new Dealer();

    MyMenu() {
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        menu.put("1", "Add car");
        menu.put("2", "Show car");
        menu.put("3", "Find car");
        menu.put("Q", "Exit");

        menuMethods.put("1", this::add);
        menuMethods.put("2", this::printCars);
        menuMethods.put("3", this::printModel);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.keySet()) {
            System.out.println(str + " = " + menu.get(str));
        }
    }

    void show() {
        String key;
        do {
            outputMenu();
            System.out.println("Enter menu point");
            key = sc.next().toUpperCase();
            try {
                menuMethods.get(key).run();
            } catch (Exception e) {
            }
        } while (!key.equals("Q"));
    }

    private void add() {
        System.out.println("Enter make");
        String make = sc.next();
        System.out.println("Enter model");
        String model = sc.next();
        System.out.println("Enter price");
        int price = sc.nextInt();
        System.out.println("Enter volume");
        double volume = sc.nextDouble();

        if (!dealer.add(new Car(model, make, price, volume))) {
            System.out.println("This car already exists, repeat please");
        }
    }

    private void printCars() {
        for (Car car : dealer.getAll())
            System.out.println(car);
        System.out.println();
    }

    private void printModel() {
        System.out.println("Enter model");
        String model = sc.next();
        dealer.findByModel(model);
    }
}
